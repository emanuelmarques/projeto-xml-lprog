<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  xmlns:ns="http://www.dei.isep.ipp.pt/lprog">
	<xsl:output method="html"/>


	<xsl:template match="/">
		<html>
			<head>
				<title>Página de Rosto</title>
				<script type="text/javascript" src="https://moodleant.isep.ipp.pt/lib/ufo.js"></script>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php"/>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" />
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php" />

				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<link rel="icon" type="image/ico" href="http://www.isep.ipp.pt/favicon.ico"/>
				<center>
					<img>
						<xsl:attribute name="src">
							<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:logotipoDEI"/>
						</xsl:attribute>
					</img>
					<p/>
					<table border = "0">
						<tr>
							<td>
								<a href="PaginaRosto.html">Página de Rosto</a>
							</td>
							<td> | </td>
							<td>
								<a href="Introducao.html">Introdução</a>
							</td>
							<td> | </td>
							<td>
								<a href="Seccoes.html">Secções</a>
							</td>
							<td> | </td>
							<td>
								<a href="Conclusao.html">Conclusão</a>
							</td>
							<td> | </td>
							<td>
								<a href="Referencias.html">Referências</a>
							</td>
							<td> | </td>
							<td>
								<a href="Anexos.html">Anexos</a>
							</td>
						</tr>
					</table>
				</center>
			</head>
			<body>

				<center>
					<h1>
						<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:tema"/>
					</h1>

					<b>Unidade Curricular: </b>
					<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:sigla"/>:
					<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:designação"/>
					(<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/@id"/>)
					<p/>

					<b>Autores:</b>
					<br/>
					<table border = "1">
						<tr>
							<xsl:for-each select="ns:relatório/ns:páginaRosto/ns:autor">
								<xsl:sort select="ns:número" data-type="number" order="ascending"/>
								<td>
									<xsl:value-of select="current()/ns:nome"/>
									(<xsl:value-of select="current()/ns:número"/>)<br/>
									<xsl:value-of select="current()/ns:mail"/>

								</td>
							</xsl:for-each>
						</tr>
					</table>
					<p/>
					<p/>
					<p/>

					<b>Turma: </b>
					<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:turmaPL"/>
					<p/>
					<b>Professor (sigla): </b>
					<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:profPL"/>
					<p/>
					<xsl:value-of select="ns:relatório/ns:páginaRosto/ns:data"/>

				</center>


			</body>

		</html>
	</xsl:template>

</xsl:stylesheet>
