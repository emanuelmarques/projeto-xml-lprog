<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>


	<xsl:template match="/">
		<html>
			<head>
				<title>Overview</title>
				<script type="text/javascript" src="https://moodleant.isep.ipp.pt/lib/ufo.js"></script>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php"/>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" />
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php" />

				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<link rel="icon" type="image/ico" href="http://www.isep.ipp.pt/favicon.ico"/>
			</head>
			<body>

				<h1>Dados Gerais</h1>
				<p/>

				<table border="0">
					<td>
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="/Operadora/DadosGerais/Logotipo"/>
							</xsl:attribute>
						</img>
					</td>

					<td>
						<b>ID da Operadora: </b>
						<xsl:value-of select="/Operadora/DadosGerais/@CodIdentificacao" />
						<br/>
						<b>Nome da Operadora: </b>
						<xsl:value-of select="/Operadora/DadosGerais/Nome" />
						<br/>
						<b>Data de inicio de informação: </b>
						<xsl:value-of select="/Operadora/DadosGerais/Datas/DataInicio/Dia" />/<xsl:value-of select="/Operadora/DadosGerais/Datas/DataInicio/Mes" />/<xsl:value-of select="/Operadora/DadosGerais/Datas/DataInicio/Ano" />
						<br/>
						<b>Data de atualização: </b>
						<xsl:value-of select="/Operadora/DadosGerais/Datas/DataAtualizacao/Dia" />/<xsl:value-of select="/Operadora/DadosGerais/Datas/DataAtualizacao/Mes" />/<xsl:value-of select="/Operadora/DadosGerais/Datas/DataAtualizacao/Ano" />
						<br/>
					</td>
				</table>


				<h1>Lista de Programas de Rádio</h1>
				<table border="1">
					<td>
						<center>
							<h6>Nome do Programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Descrição</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Emissora</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de Inicio</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de fim</h6>
						</center>
					</td>


					<xsl:for-each select="/Operadora/CanaisRadio/CanalRadio">
						<xsl:sort select="@numero" data-type="number" order="ascending"/>
						<xsl:for-each select="current()/Programas/Programa">
							<xsl:sort select="HoraInicio"/>
							<tr>
								<td>
									<xsl:value-of select="@nome" />
								</td>
								<td>
									<xsl:value-of select="Descricao" />
								</td>
								<td>
									<xsl:value-of select="../../@numero"/> -
									<xsl:value-of select="../../Nome"/>
									(<xsl:value-of select="../../Categoria"/>)
								</td>
								<td>

									<xsl:value-of select="HoraInicio" />
								</td>
								<td>

									<xsl:value-of select="HoraFim" />
								</td>

							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>


				<h1>Lista de Programas de Televisão (Episódios)</h1>
				<table border="1">
					<td>
						<center>
							<h6>Imagem do programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Nome do Programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Descrição</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Temporada</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Episódio</h6>
						</center>
					</td>

					<td>
						<center>
							<h6>Canal</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de inicio</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de Fim</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Formato</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Audio</h6>
						</center>
					</td>





					<xsl:for-each select="/Operadora/CanaisTV/CanalTV">
						<xsl:sort select="@numero" data-type="number" order="ascending"/>
						<xsl:for-each select="current()/Programas/ProgramaEp">
							<xsl:sort select="HoraInicio"/>
							<tr>
								<td>
									<center>
										<img>
											<xsl:attribute name="src">
												<xsl:value-of select="Foto"/>
											</xsl:attribute>
										</img>
									</center>
								</td>
								<td>
									<xsl:value-of select="@nome" />
								</td>
								<td>
									<xsl:value-of select="Descricao"/>
								</td>
								<td>
									<xsl:value-of select="Episodio/Temporada"/>
								</td>
								<td>
									<xsl:value-of select="Episodio/NumeroEpisodio"/>
								</td>
								<td>

									<xsl:value-of select="../../@numero"/> -
									<xsl:value-of select="../../Nome"/>
									(<xsl:value-of select="../../Categoria"/>)
								</td>
								<td>

									<xsl:value-of select="HoraInicio" />
								</td>

								<td>

									<xsl:value-of select="HoraFim" />
								</td>
								<td>

									<xsl:value-of select="FormatoImagem" />
								</td>
								<td>

									<xsl:for-each select="current()/Audio/Idioma">
										- <xsl:value-of select="self::node()"/>
										<br/>


									</xsl:for-each>
								</td>


							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>

				<h1>Lista de Programas de Televisão (Esporádicos)</h1>
				<table border="1">
					<td>
						<center>
							<h6>Imagem do programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Nome do Programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Descrição</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Canal</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de inicio</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Hora de Fim</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Formato</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Audio</h6>
						</center>
					</td>





					<xsl:for-each select="/Operadora/CanaisTV/CanalTV">
						<xsl:sort select="@numero" data-type="number" order="ascending"/>
						<xsl:for-each select="current()/Programas/ProgramaE">
							<xsl:sort select="HoraInicio"/>
							<tr>
								<td>
									<center>
										<img>
											<xsl:attribute name="src">
												<xsl:value-of select="Foto"/>
											</xsl:attribute>
										</img>
									</center>
								</td>
								<td>
									<xsl:value-of select="@nome" />
								</td>
								<td>
									<xsl:value-of select="Descricao"/>
								</td>
								<td>

									<xsl:value-of select="../../@numero"/> -
									<xsl:value-of select="../../Nome"/>
									(<xsl:value-of select="../../Categoria"/>)
								</td>
								<td>

									<xsl:value-of select="HoraInicio" />
								</td>

								<td>

									<xsl:value-of select="HoraFim" />
								</td>
								<td>

									<xsl:value-of select="FormatoImagem" />
								</td>
								<td>

									<xsl:for-each select="current()/Audio/Idioma">
										- <xsl:value-of select="self::node()"/>
										<br/>


									</xsl:for-each>
								</td>


							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>

				<h1>Lista de Gravações</h1>
				<table border="1">
					<td>
						<center>
							<h6>ID</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Nome do Programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Data</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Duração</h6>
						</center>
					</td>


					<xsl:for-each select="/Operadora/Gravacoes/Gravacao">
						<xsl:sort select="@id" data-type="number" order="ascending"/>
						<tr>
							<td>
								<xsl:value-of select="@id" />
							</td>
							<td>
								<xsl:value-of select="NomePrograma" />
							</td>
							<td>
								<xsl:value-of select="current()/DataGravacao/Dia"/>/<xsl:value-of select="current()/DataGravacao/Mes"/>/<xsl:value-of select="current()/DataGravacao/Ano"/>
							</td>
							<td>

								<xsl:value-of select="Duracao" />
							</td>


						</tr>
					</xsl:for-each>
				</table>

				<h1>Lista de Filmes no Videoclube</h1>
				<table border="1">
					<td>
						<center>
							<h6>Título</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Ano</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Descrição</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Género</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Realizador</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Atores Principais</h6>
						</center>
					</td>


					<xsl:for-each select="/Operadora/Videoclube/Filme">
						<xsl:sort select="Ano" data-type="number" order="ascending"/>
						<tr>
							<td>
								<xsl:value-of select="Título" />
							</td>
							<td>
								<xsl:value-of select="Ano" />
							</td>
							<td>
								<xsl:value-of select="Descricao" />
							</td>
							<td>


								<xsl:for-each select="current()/Generos/Genero">

									- <xsl:value-of select="self::node()"/>
									<br/>

								</xsl:for-each>


							</td>

							<td>
								<xsl:value-of select="Realizador" />
							</td>
							<td>
								<xsl:for-each select="current()/AtoresPrincipais/Ator">
									- <xsl:value-of select="self::node()"/>
									<br/>


								</xsl:for-each>
							</td>


						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
