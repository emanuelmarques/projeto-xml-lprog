<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml"/>


	<xsl:template match="/">
		<ProgramasTV>
			<xsl:for-each select="/Operadora/CanaisTV/CanalTV">
				<xsl:sort select="@id" data-type="number" order="ascending"/>
				<xsl:for-each select="current()/Programas/ProgramaEp">
					<ProgramaTV>
						<xsl:attribute name="nome">
							<xsl:value-of select="@nome"/>
						</xsl:attribute>
						<Foto>
							<xsl:value-of select="Foto"/>
						</Foto>
						<Descricao>
							<xsl:value-of select="Descricao"/>
						</Descricao>
						<Episodio>
							<Temporada>
								<xsl:value-of select="current()/Episodio/Temporada"/>
							</Temporada>
							<NumeroEpisodio>
								<xsl:value-of select="current()/Episodio/NumeroEpisodio"/>
							</NumeroEpisodio>
						</Episodio>
						<Canal>
							<xsl:value-of select="../../Nome"/>
						</Canal>
						<Formato>
							<xsl:value-of select="FormatoImagem"/>
						</Formato>
						<Audio>
							<xsl:for-each select="current()/Audio/Idioma">
								<Idioma>
									<xsl:value-of select="self::node()"/>
								</Idioma>
							</xsl:for-each>
						</Audio>

					</ProgramaTV>
				</xsl:for-each>

				<xsl:for-each select="current()/Programas/ProgramaE">
					<ProgramaTV>
						<xsl:attribute name="nome">
							<xsl:value-of select="@nome"/>
						</xsl:attribute>
						<Foto>
							<xsl:value-of select="Foto"/>
						</Foto>
						<Descricao>
							<xsl:value-of select="Descricao"/>
						</Descricao>

						<Canal>
							<xsl:value-of select="../../Nome"/>
						</Canal>
						<Formato>
							<xsl:value-of select="FormatoImagem"/>
						</Formato>
						<Audio>
							<xsl:for-each select="current()/Audio/Idioma">
								<Idioma>
									<xsl:value-of select="self::node()"/>
								</Idioma>
							</xsl:for-each>
						</Audio>

					</ProgramaTV>
				</xsl:for-each>

			</xsl:for-each>
		</ProgramasTV>

	</xsl:template>

</xsl:stylesheet>
