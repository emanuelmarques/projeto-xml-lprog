<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>Lista de Gravações</title>
				<script type="text/javascript" src="https://moodleant.isep.ipp.pt/lib/ufo.js"></script>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php"/>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" />
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php" />

				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<link rel="icon" type="image/ico" href="http://www.isep.ipp.pt/favicon.ico"/>

			</head>
			<body>

				<table border="1">
					<td>
						<center>
							<h6>ID</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Nome do Programa</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Data</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Duração</h6>
						</center>
					</td>


					<xsl:for-each select="/Operadora/Gravacoes/Gravacao">
						<tr>
							<td>
								<xsl:value-of select="@id" />
							</td>
							<td>
								<xsl:value-of select="NomePrograma" />
							</td>
							<td>
								<xsl:value-of select="current()/DataGravacao/Dia"/>/<xsl:value-of select="current()/DataGravacao/Mes"/>/<xsl:value-of select="current()/DataGravacao/Ano"/>
							</td>
							<td>

								<xsl:value-of select="Duracao" />
							</td>


						</tr>
					</xsl:for-each>
				</table>

			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
