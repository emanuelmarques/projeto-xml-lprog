<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml"/>


	<xsl:template match="/">
		<CanaisRadio>
			<xsl:for-each select="/Operadora/CanaisRadio/CanalRadio">
				<xsl:sort select="@numero" data-type="number" order="ascending"/>
				<CanalRadio>
					<xsl:attribute name="numero">
						<xsl:value-of select="@numero"/>
					</xsl:attribute>
					<Nome>
						<xsl:value-of select="Nome"/>
					</Nome>
					<Categoria>
						<xsl:value-of select="Categoria"/>
					</Categoria>

					<Programas>
						<xsl:for-each select="current()/Programas/Programa">
							<Programa >
								<xsl:attribute name="nome">
									<xsl:value-of select="@nome"/>
								</xsl:attribute>
								<HoraInicio>
									<xsl:value-of select="HoraInicio"/>
								</HoraInicio>
								<HoraFim>
									<xsl:value-of select="HoraFim"/>
								</HoraFim>
								<Descricao>
									<xsl:value-of select="Descricao"/>
								</Descricao>
							</Programa>
						</xsl:for-each>
					</Programas>

				</CanalRadio>

			</xsl:for-each>
		</CanaisRadio>
	</xsl:template>

</xsl:stylesheet>
