<?xml version="1.0" encoding="UTF-8"?>

<!-- Trabalho XML LPROG
		Emanuel Marques - 1130553
		Miguel Marques - 1130485
		Eduardo Pinto - 1130466
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:template match="/">
		<html>
			<head>
				<title>Lista de Filmes no Videoclube</title>
				<script type="text/javascript" src="https://moodleant.isep.ipp.pt/lib/ufo.js"></script>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php"/>
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" />
				<link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php" />

				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<link rel="icon" type="image/ico" href="http://www.isep.ipp.pt/favicon.ico"/>
			</head>
			<body>
				<table border="1">
					<td>
						<center>
							<h6>Título</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Ano</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Descrição</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Género</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Realizador</h6>
						</center>
					</td>
					<td>
						<center>
							<h6>Atores Principais</h6>
						</center>
					</td>


					<xsl:for-each select="/Operadora/Videoclube/Filme">
						<tr>
							<td>
								<xsl:value-of select="Título" />
							</td>
							<td>
								<xsl:value-of select="Ano" />
							</td>
							<td>
								<xsl:value-of select="Descricao" />
							</td>
							<td>


								<xsl:for-each select="current()/Generos/Genero">

									- <xsl:value-of select="self::node()"/>
									<br/>

								</xsl:for-each>


							</td>

							<td>
								<xsl:value-of select="Realizador" />
							</td>
							<td>
								<xsl:for-each select="current()/AtoresPrincipais/Ator">
									- <xsl:value-of select="self::node()"/>
									<br/>


								</xsl:for-each>
							</td>


						</tr>
					</xsl:for-each>
				</table>

			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
